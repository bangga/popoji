<?php
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://pdam.tirtasakti.co.id');

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	// https://mahoni.simeut.my.id:8080/tirtasakti-info-tagihan?data=015414&draw=0&length=2&start=1
	curl_setopt($ch, CURLOPT_URL, 'https://mahoni.simeut.my.id:8080/tirtasakti-info-tagihan?data='.$_GET['data'].'&draw='.$_POST['draw'].'&length='.$_POST['length'].'&start='.$_POST['start']);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	echo curl_exec($ch);

	// close cURL resource, and free up system resources
	curl_close($ch);


        flush();
