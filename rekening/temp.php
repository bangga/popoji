<?php
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://pdam.tirtasakti.co.id');

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'http://sopp-devel.tirtasakti.co.id/core-data/server');

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	echo curl_exec($ch);
	
	// close cURL resource, and free up system resources
	curl_close($ch);


    flush();
?>
