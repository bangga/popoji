<?php
	/** input
	
		url :http://pdam.tirtaintan.co.id/payment/7B22746F6B656E223A2235383064366366363236616636222C2264617461223A5B7B2272656B5F6E6F6D6F72223A22323031363039313530303033343534222C2272656B5F746F74616C223A223636323034222C2273657269616C223A22303030303031222C226279725F74676C223A223230313631303234222C226C6F6B6574223A2258584445563031227D5D7D
		
		payload :
		object(stdClass)#1 (2) { ["token"]=> string(13) "580d6cf626af6" ["data"]=> array(1) { [0]=> object(stdClass)#2 (5) { ["rek_nomor"]=> string(15) "201609150003454" ["rek_total"]=> string(5) "66204" ["serial"]=> string(6) "000001" ["byr_tgl"]=> string(8) "20161024" ["loket"]=> string(7) "XXDEV01" } } }
	*/

	/** output
		{"token": "57fc7b7762098", "errno": "0", "error": "1 transaksi berhasil"}
		{"token": "57fc7b7762098", "errno": "1", "error": "terjadi gangguan teknis"}
		{"token": "57fc7b7762098", "errno": "2", "error": "sesi telah berakhir"}
		{"token": "57fc7b7762098", "errno": "3", "error": "tagihan telah dibayar di loket xxx pada tanggal 2016-05-20 10:11:09"}
	*/

	header('Content-Type: application/json');

	function put_log($raw_data){
		$fp = fopen('data_bayar.log','a');
		fwrite($fp, $raw_data."\n");
		fclose($fp);
	}
	
	$raw_input 	= hex2bin($_GET['data']);
	
	put_log($raw_input);
	
	$raw_proses	= (array) json_decode($raw_input);
	if(count($raw_proses['data'])>0){
		$raw_respon	= array("token" => $raw_proses['token'], "errno" => 0, "error" => count($raw_proses['data'])." transaksi berhasil");
	}
	else{
		$raw_respon	= array("token" => $raw_proses['token'], "errno" => 2, "error" => "sesi telah berakhir");
	}

	echo json_encode($raw_respon)."\n";
	flush();
?>
