<?php
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: https://pdam.tirtasakti.co.id');

	// create a new cURL resource
	$ch = curl_init();

	// define data post
	$data_post = array(
		'SERVER_PROTOCOL' => $_SERVER['SERVER_PROTOCOL'],
		'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'],
		'HTTP_X_REAL_IP' => $_SERVER['REMOTE_ADDR'],
		'HTTPS' => $_SERVER['HTTPS'],
		'SSL_SESSION_ID' => $_SERVER['SSL_SESSION_ID'],
		'SSL_CIPHER' => $_SERVER['SSL_CIPHER'],
		'REDIRECT_STATUS' => $_SERVER['REDIRECT_STATUS']
	);

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'https://10.10.26.36/tirtasakti-core');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_post));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	//echo curl_exec($ch);
	echo json_encode($_SERVER);
	
	// close cURL resource, and free up system resources
	curl_close($ch);

	flush();
