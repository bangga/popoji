<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
	<!-- Breadcrumb -->
	<section class="breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?=$currentPag->title;?></h1>
					<ol class="breadcrumb bc-3">
						<li><a href="<?=$website_url;?>"><i class="entypo-home"></i>Home</a></li>
						<li class="active"><strong>Info Tagihan</strong></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<!-- About Us Text -->
	<section class="content-section">
		<div class="container">
			<div class="row vspace">
				<div class="col-md-12">
					<h4>Info Tagihan</h4>
					<p>Informasi tagihan rekening air</p>
					<div class="callout-action">
						<h2><input id="pel_no" type="text" name="pel_no" class="form-control" maxlength="6" placeholder="Masukan 6 Digit Nomer Sambungan" /></h2>
						<div class="callout-button">
							<button class="btn btn-secondary">Periksa</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row vspace">
				<div class="col-md-12"><h4 id="pel_nama"></h4></div>
				<div class="col-md-12"><p id="pel_alamat"></p></div>
				<div class="col-md-12"><p id="cabang"></p></div>
			</div>
			<div class="row vspace">
				<div class="col-md-12">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-tagihan"></table>
					</div>
				</div>
			</div>
		</div>
	</section>
		
    <script>
		$(document).ready(function() {
			var dataTemp	= {};
			var dataProc	= {};
			var dataFeed	= {};
			
			$('button').click(function(){
				var param 		= $('#pel_no').val();
				var dataFeed	= {pel_no: param};
				localStorage.setItem('tirtasakti', JSON.stringify(dataFeed));
				document.location.href	= '/info-tagihan';
			});

			if(typeof(localStorage.tirtasakti)=='string'){
				dataTemp 	= JSON.parse(localStorage.tirtasakti);
				$('#dataTables-tagihan').DataTable({
					responsive: true,
					searching: false,
					processing: true,
					serverSide: true,
					ajax: {
						url: 'https://pdam.tirtasakti.co.id/rekening/view_rinci.php?data=' + dataTemp.pel_no,
						type: 'POST',
						data: {filter: [{name: 'pel_no', value: dataTemp.pel_no}]},
						dataFilter: function(data){
							dataProc = $.parseJSON(data);
							if(dataProc.data.length>0){
								dataFeed = dataProc.data[0];
								$('#pel_no').attr('placeholder', 'Nomer Sambungan ' + dataFeed.pel_no);
								$('#pel_nama').html(dataFeed.pel_nama);
								$('#pel_alamat').html(dataFeed.pel_alamat);
								$('#cabang').html('Cabang Pelayanan ' + dataFeed.cabang);
								localStorage.setItem('tirtasakti', JSON.stringify(dataFeed));
							}
							else{
								if(typeof(dataTemp.pel_no)=='string'){
									$('#pel_no').attr('placeholder', 'Nomer Sambungan ' + dataTemp.pel_no);									
								}
								$('#pel_nama').html('Data pelanggan tidak ditemukan');
								$('#pel_alamat').html('Periksa kembali nomer sambungan yang anda cari');
							}
							return JSON.stringify(dataProc);
						}
					},
					columns: [
						{ title: "Bulan", data: "rek_bln", className: "text-right" },
						//{ title: "Stanlalu", data: "rek_stanlalu", className: "hidden-xs text-right" },
						//{ title: "Stankini", data: "rek_stankini", className: "hidden-xs text-right" },
						{ title: "Pemakaian", data: "rek_pakai", className: "hidden-xs text-right" },
						//{ title: "Biaya Air", data: "rek_uangair", className: "hidden-xs text-right" },
						//{ title: "Biaya Beban", data: "rek_beban", className: "hidden-xs text-right" },
						{ title: "Total", data: "rek_total", className: "text-right" },
						{ title: "Dibayar", data: "byr_tgl", className: "hidden-xs text-right" },
						{ title: "Loket", data: "byr_loket", className: "col-md-2" }
					]
				});
			}
		});
    </script>
<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>
