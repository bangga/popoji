<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
		<!-- Footer Widgets -->
		<section class="footer-widgets">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<h5>Alamat</h5>
						<p>Jln. Siak Lengih<br />Kota Sungai Penuh, Provinsi Jambi<br />Indonesia</p>
					</div>
					<div class="col-sm-3">
						<h5>Kontak</h5>
						<p>Telepon: +62 0748-324047<br />info@tirtasakti.co.id</p>						<ul class="social-networks">							<li><a href="#"><i class="entypo-instagram"></i></a>							</li>							<li><a href="#"><i class="entypo-twitter"></i></a>							</li>							<li><a href="#"><i class="entypo-facebook"></i></a>							</li>						</ul>
					</div>
				</div>
			</div>
		</section>

		<!-- Site Footer -->
		<footer class="site-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						Copyright &copy; 2013-2015 Neon - All Rights Reserved. 
					</div>
					<div class="col-sm-6">
						<ul class="social-networks text-right">
							<li><a href="#"><i class="entypo-instagram"></i></a></li>
							<li><a href="#"><i class="entypo-twitter"></i></a></li>
							<li><a href="#"><i class="entypo-facebook"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</div>

    <!-- JavaScript -->
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/gsap/main-gsap.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/bootstrap.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/joinable.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/resizeable.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/neon-slider.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/neon-custom.js"></script>
		
    <!-- DataTables JavaScript -->
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/dataTables/jquery.dataTables.js"></script>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/assets/js/dataTables/dataTables.bootstrap.js"></script>
</body>
</html>
<?php } ?>
