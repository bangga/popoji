<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
	<!-- Breadcrumb -->
	<section class="breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?=$currentPag->title;?></h1>
					<ol class="breadcrumb bc-3">
						<li><a href="<?=$website_url;?>"><i class="entypo-home"></i>Home</a></li>
						<li class="active"><strong>Simulasi Tarif</strong></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	
	<section id="content">
		<div class="mb30"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<input id="rek-pakai" name="rek-pakai" type="text" class="form-control hitung-tarif" placeholder="Masukan Pemakaian Air (M3)" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<select name="client-grup" class="form-control hitung-tarif">
							<option id="00" value="00">&nbsp;Pilih Golongan Tarif</option>
							<option id="SU" value="SU">&nbsp;Sosial Umum</option>
							<option id="SK" value="SK">&nbsp;Sosial Khusus</option>
							<option id="R1" value="R1">&nbsp;Rumah Tangga 1</option>
							<option id="R2" value="R2">&nbsp;Rumah Tangga 2</option>
							<option id="IP" value="IP">&nbsp;Instansi Pemerintah</option>
							<option id="NK" value="NK">&nbsp;Niaga Kecil</option>
							<option id="NB" value="NB">&nbsp;Niaga Besar</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<button class="btn btn-secondary">Periksa</button>
					</div>
				</div>
			</div>
			<div class="row">
				<table id="tabel-tarif" class="table hidden">
					<thead>
						<tr>
							<th class="hidden-xs">Segmentasi (M3)</th>
							<th>Pemakaian (M3)</th>
							<th class="hidden-xs">Harga Air (Rp/M3)</th>
							<th>Total (Rp)</th>
						</tr>
					</thead>
					<tbody id="body-tarif"></tbody>
					<tfoot id="foot-tarif"></tfoot>
				</table>
			</div>
		</div>
	</section>

<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>

    <!-- number formater -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/numeral/jshashtable-2.1.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/numeral/jquery.numberformatter-1.2.3.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/numeral/numeral.min.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/numeral/numeral.de-de.js"></script>

	<script>
		$(document).ready(function() {
			$('button').click(function(){
				var dataTemp = {};
				var dataFeed = {};
				var dataProc = $('.hitung-tarif').serializeArray();
				
				if(typeof(localStorage.tirtasakti)=='string'){
					dataTemp = JSON.parse(localStorage.tirtasakti);
				}
				else{
					dataTemp.rek_pakai = 0;
				}
				
				if(parseInt(dataProc[0].value)>0){
					dataTemp.rek_pakai = dataProc[0].value;
				}
				
				if(dataTemp.rek_pakai>0){
					dataTemp.gol_kel	= dataProc[1].value;
					localStorage.setItem('tirtasakti', JSON.stringify(dataTemp));
					document.location.href  = '/hitung-tarif';
				}
				else{
					alert('Volume pemakaian air belum diisi');
				}
			});
		});
	</script>

	<script>
		(function() {
			var dataTemp 	= {};
			var inHTML1		= "";
			var inHTML2		= "";
			var inHTML3		= "";
			var inHTML4		= "";
			var inHTML5		= "";
			var tar_sd1		= 0;
			var tar_sd2		= 0;
			var tar_sd3		= 0;
			var tar_sd4		= 0;
			var tar_hd1		= 0;
			var tar_hd2		= 0;
			var tar_hd3		= 0;
			var tar_hd4		= 0;
			var rek_pakai	= 0;
			var tar_adm		= 0;
			var tot_pakai	= 0;
			var tot_harga	= 0;
			var segmen		= "";
			if(typeof(localStorage.tirtasakti)=='string'){
				$('#tabel-tarif').removeClass('hidden');
				dataTemp = JSON.parse(localStorage.tirtasakti);
				$('#' + dataTemp.gol_kel).attr('selected', true);
				if(typeof(dataTemp.rek_pakai)=='string'){
					$('#rek-pakai').attr('placeholder', 'Pemakaian Air ' + dataTemp.rek_pakai + ' (M3)');
				}
				rek_pakai	= parseInt(dataTemp.rek_pakai);
				tot_pakai	= rek_pakai;

				switch(dataTemp.gol_kel){

/*
select gol_kode,tar_1,tar_2,tar_3,tar_4,tar_5,tar_6,tar_7,tar_8,tar_sd1,tar_sd2,tar_sd3,tar_sd4,tar_sd5,tar_sd6,tar_sd7,tar_adm from tm_kode_tarif where tar_bln_akhir=202012;
+----------+-------+-------+-------+-------+-------+-------+-------+-------+---------+---------+---------+---------+---------+---------+---------+---------+
| gol_kode | tar_1 | tar_2 | tar_3 | tar_4 | tar_5 | tar_6 | tar_7 | tar_8 | tar_sd1 | tar_sd2 | tar_sd3 | tar_sd4 | tar_sd5 | tar_sd6 | tar_sd7 | tar_adm |
+----------+-------+-------+-------+-------+-------+-------+-------+-------+---------+---------+---------+---------+---------+---------+---------+---------+
| SU       |  1300 |  1300 |  1300 |  1300 |  1300 |  1300 |  1300 |  1300 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    5000 |
| SK       |  1300 |  1700 |  2000 |  2000 |  2000 |  2000 |  2000 |  2000 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    5000 |
*/
					case 'SU':
						tar_sd1 = 10;
						tar_sd2 = 0;
						tar_sd3 = 0;
						tar_sd4 = 0;
						tar_hd1 = 1300;
						tar_hd2 = 1300;
						tar_hd3 = 1300;
						tar_hd4 = 1300;
						tar_adm = 7000;
						break;
					case 'SK':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 1300;
						tar_hd2 = 1700;
						tar_hd3 = 2000;
						tar_hd4 = 2000;
						tar_adm = 7000;
						break;
/*
| R1       |  1700 |  2500 |  3400 |  3400 |  3400 |  3400 |  3400 |  3400 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
| R2       |  2500 |  3400 |  4200 |  4200 |  4200 |  4200 |  4200 |  4200 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
*/
					case 'R1':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 1700;
						tar_hd2 = 2500;
						tar_hd3 = 3400;
						tar_hd4 = 3400;
						tar_adm = 10000;
						break;
					case 'R2':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 2500;
						tar_hd2 = 3400;
						tar_hd3 = 4200;
						tar_hd4 = 4200;
						tar_adm = 10000;
						break;
					case 'R3':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 3100;
						tar_hd2 = 3900;
						tar_hd3 = 4800;
						tar_hd4 = 4800;
						tar_adm = 10000;
						break;

/*						
| R3       |  3100 |  3900 |  4800 |  4800 |  4800 |  4800 |  4800 |  4800 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
| IP       |  3400 |  4200 |  5100 |  5100 |  5100 |  5100 |  5100 |  5100 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
| NK       |  4200 |  5100 |  6700 |  6700 |  6700 |  6700 |  6700 |  6700 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
*/
						
					case 'IP':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 3400;
						tar_hd2 = 4200;
						tar_hd3 = 5100;
						tar_hd4 = 5100;
						tar_adm = 10000;
						break;
					case 'NK':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 4200;
						tar_hd2 = 5100;
						tar_hd3 = 6700;
						tar_hd4 = 6700;
						tar_adm = 10000;
						break;
						
/*
| NB       |  5100 |  6700 |  8400 |  8400 |  8400 |  8400 |  8400 |  8400 |      10 |      20 |      30 |      40 |      50 |      60 |      70 |    8000 |
+----------+-------+-------+-------+-------+-------+-------+-------+-------+---------+---------+---------+---------+---------+---------+---------+---------+
8 rows in set (0.00 sec)
*/
						
					case 'NB':
						tar_sd1 = 10;
						tar_sd2 = 20;
						tar_sd3 = 30;
						tar_sd4 = 0;
						tar_hd1 = 5100;
						tar_hd2 = 6700;
						tar_hd3 = 8400;
						tar_hd4 = 8400;
						tar_adm = 10000;
						break;
					default:
				}

				if(tar_sd4>0 && rek_pakai>tar_sd3){
					inHTML4 = '<tr>' +
						'<td class="hidden-xs">>' + tar_sd3 + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd3) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd4, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd4*(rek_pakai-tar_sd3), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga 	= tar_hd4*(rek_pakai-tar_sd3);
					rek_pakai 	= tar_sd3;
					segmen		= (tar_sd2+1) + ' - ' + tar_sd3;
				}
				else{
					segmen = '> ' + tar_sd2;
				}
				if(tar_sd3>0 && rek_pakai>tar_sd2){
					inHTML3 = '<tr>' +
						'<td class="hidden-xs">' + segmen + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd2) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd3, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd3*(rek_pakai-tar_sd2), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga 	= tot_harga + tar_hd3*(rek_pakai-tar_sd2);
					rek_pakai 	= tar_sd2;
					segmen		= (tar_sd1+1) + ' - ' + tar_sd2;
				}
				else{
					segmen		= '> ' + tar_sd1;
				}
				if(tar_sd2>0 && rek_pakai>tar_sd1){
					inHTML2 = '<tr>' +
						'<td class="hidden-xs">' + segmen + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd1) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd2, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd2*(rek_pakai-tar_sd1), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga 	= tot_harga + tar_hd2*(rek_pakai-tar_sd1);
					rek_pakai 	= tar_sd1;
					segmen		= '1 - ' + tar_sd1;
				}
				else{
					segmen		= '> 1';
				}
				inHTML1 	= '<tr>' +
						'<td class="hidden-xs">' + segmen + '</td>' +
						'<td class="text-right">' + rek_pakai + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd1, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd1*rek_pakai, {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
				tot_harga	= tot_harga + tar_hd1*rek_pakai;
				// tabel footer
				inHTML5 	= '<tr>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + tot_pakai + '</td>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + jQuery.formatNumber(tot_harga, {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">Biaya Beban</td>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_adm, {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">Total Tagihan</td>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + jQuery.formatNumber((tot_harga+tar_adm), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
				
				jQuery('#body-tarif').html(inHTML1 + inHTML2 + inHTML3 + inHTML4);
				jQuery('#foot-tarif').html(inHTML5);
			}
		})();
	</script>
